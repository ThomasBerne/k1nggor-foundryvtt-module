import utils from './utils.js';

const guerrier = utils.featuresToClass({
  1: [
    `${utils.CLASSE_PREFIX}joes0AoXtTzCEeT8`,
    `${utils.CLASSE_PREFIX}5W7jfRAvnAWsquE3`,
  ],
  2: [
    `${utils.CLASSE_PREFIX}21zucFmiuBPmCbp0`,
  ],
  3: [
    `${utils.CLASSE_PREFIX}BeVjcoMIbjp0VYdJ`,
  ],
  4: [
    `${utils.CLASSE_PREFIX}OWP1Ji1GpR21p7Of`,
  ],
  5: [
    `${utils.CLASSE_PREFIX}PDPDQ2BCA2nTsu5q`,
  ],
  9: [
    `${utils.CLASSE_PREFIX}VSZxKGZWh9341UPa`,
  ],
});

export default {
  guerrier,
  'guerrier (archer arcanique)': utils.concatenateClassFeatures(guerrier, {
    3: [
      `${utils.CLASSE_PREFIX}ltTjcuaOa04m7JFQ`,
      `${utils.CLASSE_PREFIX}PcSoRImjhnpo4QIF`,
    ],
    7: [
      `${utils.CLASSE_PREFIX}Qy7OMx3R6zLxC8Rq`,
      `${utils.CLASSE_PREFIX}sLnwsSGcu9kkjbtR`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}jHy2Iufuu4pNmjY9`,
    ],
  }),
  'guerrier (maître de guerre)': utils.concatenateClassFeatures(guerrier, {
    3: [
      `${utils.CLASSE_PREFIX}gl6VW2ipTRTjaKmz`,
      `${utils.CLASSE_PREFIX}55iKENv4TbwLwiAF`,
    ],
    7: [
      `${utils.CLASSE_PREFIX}Pd5PHKBn8a9wo0nh`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}bSLC0IhgESvvq93p`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}Um0jYoGrC5kr2gbA`,
    ],
  }),
  'guerrier (chevalier)': utils.concatenateClassFeatures(guerrier, {
    3: [
      `${utils.CLASSE_PREFIX}uHlqnQce53L784If`,
      `${utils.CLASSE_PREFIX}gy4y7mV5mjzpcNNa`,
      `${utils.CLASSE_PREFIX}ZuqXctio3RPsM3q0`,
    ],
    7: [
      `${utils.CLASSE_PREFIX}xl2pgMLzsoEpNq99`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}VQJmDdQLwMhvK8ET`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}MEJSgCfRU3rqiq0M`,
    ],
    18: [
      `${utils.CLASSE_PREFIX}JNRDqhqwbjt5PSS2`,
    ],
  }),
  'guerrier (champion)': utils.concatenateClassFeatures(guerrier, {
    3: [
      `${utils.CLASSE_PREFIX}jLA1EAqRO7jPH32q`,
    ],
    7: [
      `${utils.CLASSE_PREFIX}A9bUBoHdLDfhkrMD`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}tIrNR5gCOoNZ50AL`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}mFzHtn5m6bP4hKVI`,
    ],
    18: [
      `${utils.CLASSE_PREFIX}a0iiUeux0iqLphS7`,
    ],
  }),
  'guerrier (samouraï)': utils.concatenateClassFeatures(guerrier, {
    3: [
      `${utils.CLASSE_PREFIX}Vz1XxViBP13CX2VI`,
      `${utils.CLASSE_PREFIX}T0KjwnoDjL5VtF3w`,
    ],
    7: [
      `${utils.CLASSE_PREFIX}Yy9Dj74FR54942cH`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}Yd7ObkwHELRXSWBe`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}kbRatAhnnmQrHorb`,
    ],
    18: [
      `${utils.CLASSE_PREFIX}9K6ClzUcI5xt0oa2`,
    ],
  }),
  'guerrier (chevalier du dragon pourpre)': utils.concatenateClassFeatures(
    guerrier,
    {
      3: [
        `${utils.CLASSE_PREFIX}EhFmaWZIYRsrCihc`,
      ],
      7: [
        `${utils.CLASSE_PREFIX}qydwei8EuZN4DJk7`,
      ],
      10: [
        `${utils.CLASSE_PREFIX}YLQSZI2CHeP6VfRy`,
      ],
      15: [
        `${utils.CLASSE_PREFIX}sEEndsFQy4CuPdNV`,
      ],
    },
  ),
  'guerrier (chevalier occulte)': utils.concatenateClassFeatures(guerrier, {
    3: [
      `${utils.CLASSE_PREFIX}c9LjEFN81hJNzjFJ`,
      `${utils.CLASSE_PREFIX}SJw1M09xLy0JmBFD`,
    ],
    7: [
      `${utils.CLASSE_PREFIX}8ZjnXeBRLeQKjL6e`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}ItpYYorFgdI0kl1R`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}VI1DrqGG8vd3QNg6`,
    ],
    18: [
      `${utils.CLASSE_PREFIX}bpvQqHFidhTxH6zh`,
    ],
  }),
  "guerrier (tireur d'élite)": utils.concatenateClassFeatures(guerrier, {
    3: [
      `${utils.CLASSE_PREFIX}RoK1UzCGVaoI2Drk`,
      `${utils.CLASSE_PREFIX}SVWlrayfeqrAZPQV`,
      `${utils.CLASSE_PREFIX}lr1DwECW0Hk1sWsj`,
    ],
    7: [
      `${utils.CLASSE_PREFIX}0vXB0Vf9wF9HmO1T`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}umuiAR9whPY48slx`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}r01hpRMbHhYxktNH`,
    ],
    18: [
      `${utils.CLASSE_PREFIX}LpIogOm7MpHdqBfD`,
    ],
  }),
  'guerrier (chevalier runique)': utils.concatenateClassFeatures(guerrier, {
    3: [
      `${utils.CLASSE_PREFIX}VsZVn4Rxo6diHcVi`,
      `${utils.CLASSE_PREFIX}bHlTHMBRBdOwBGRM`,
      `${utils.CLASSE_PREFIX}C32LwxF49TADRxfu`,
    ],
    7: [
      `${utils.CLASSE_PREFIX}0SD8jkdyt2A26qfr`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}zllO4jWyRL5fl27E`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}Q6QU8iVDH9r5ISU3`,
    ],
    18: [
      `${utils.CLASSE_PREFIX}P3zczztAlykNc86Z`,
    ],
  }),
};
