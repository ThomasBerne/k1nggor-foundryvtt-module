import utils from './utils.js';

const druide = utils.featuresToClass({
  1: [
    `${utils.CLASSE_PREFIX}nQr1apzdvi9WPFwJ`,
    `${utils.CLASSE_PREFIX}43lF4zbNblxGloIH`,
  ],
  2: [
    `${utils.CLASSE_PREFIX}QUjbA5b7JNYyX3kk`,
    `${utils.CLASSE_PREFIX}bFhrlTuLbvxS1mDd`,
  ],
  4: [
    `${utils.CLASSE_PREFIX}G385iJnHjefiSrds`,
  ],
  18: [
    `${utils.CLASSE_PREFIX}wz1h1041t5uRrjDo`,
    `${utils.CLASSE_PREFIX}glXsuoFq1JZ4YWmc`,
  ],
  20: [
    `${utils.CLASSE_PREFIX}boKwUoHXTA15RTLV`,
  ],
});

export default {
  ...druide,
  'druide (cercle des rêves)': utils.concatenateClassFeatures(druide, {
    2: [
      `${utils.CLASSE_PREFIX}INZqXrAgfihXLDCQ`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}nyCgM73Y7E7IZdC7`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}Ezf9kDW6ssKehvTe`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}gQA3cE5mjgVpnnQA`,
    ],
  }),
  'druide (cercle de la lune)': utils.concatenateClassFeatures(druide, {
    2: [
      `${utils.CLASSE_PREFIX}3UvGA3jvudlcMnAV`,
      `${utils.CLASSE_PREFIX}pYcTzDhIfV49echs`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}QKJ0qApmHXNjWUck`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}XaO6untm91xJNi9w`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}Zb0vb8y8wwvXBkRR`,
    ],
  }),
  'druide (cercle de la terre)': utils.concatenateClassFeatures(druide, {
    2: [
      `${utils.CLASSE_PREFIX}LijtL0ebfnnu0Eer`,
      `${utils.CLASSE_PREFIX}tKPv7vXXXIjwtGiq`,
    ],
    3: [
      `${utils.CLASSE_PREFIX}wuo31QTTMa5GBKye`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}8yPCouzEW05cdtWz`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}wTBpiIPViOYkaENH`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}EmZZrwAUiRhAutE1`,
    ],
  }),
  'druide (cercle du berger)': utils.concatenateClassFeatures(druide, {
    2: [
      `${utils.CLASSE_PREFIX}DYtTTlitjEByqC1r`,
      `${utils.CLASSE_PREFIX}F0lXkhaoECooqFgW`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}wX0fvutR7vB1K5y7`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}jEHUsK17Lcj4HNFv`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}fCbFOyZlm1BABp1H`,
    ],
  }),
  'druide (cercle des feux rugissants)': utils.concatenateClassFeatures(
    druide,
    {
      2: [
        `${utils.CLASSE_PREFIX}ZZvszA476oR70HXo`,
        `${utils.CLASSE_PREFIX}k6rYjL7i8QdNu5Lt`,
      ],
      6: [
        `${utils.CLASSE_PREFIX}Jc5v6Sk1abpr83FS`,
      ],
      10: [
        `${utils.CLASSE_PREFIX}G0MHz8xATx3JBbDU`,
      ],
      14: [
        `${utils.CLASSE_PREFIX}tHQeoe5aMbDSadzA`,
      ],
    },
  ),
  'druide (cercle des spores)': utils.concatenateClassFeatures(druide, {
    2: [
      `${utils.CLASSE_PREFIX}c4HXeO1lM4tM16E6`,
      `${utils.CLASSE_PREFIX}WExZe1TbRZGgVDIi`,
      `${utils.CLASSE_PREFIX}MNtBe0gU0lxQ6eoC`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}Yat0529gyRlE1tPE`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}GDB9Y4nXP5kMIW8K`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}BprEziBspQE8fPD4`,
    ],
  }),
  'druide (cercle des étoiles)': utils.concatenateClassFeatures(druide, {
    2: [
      `${utils.CLASSE_PREFIX}PbmnlqaRvUy62Owx`,
      `${utils.CLASSE_PREFIX}dNQnj2EHuVlGPyeX`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}sBMCYBtVqZBp1hBj`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}hWl7VVGh4Nepjeqg`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}BULYic6eIMLaIWqN`,
    ],
  }),
};
