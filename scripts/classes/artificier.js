import utils from './utils.js';

const artificier = utils.featuresToClass({
  1: [
    `${utils.CLASSE_PREFIX}dBlVAOZOvfj4MGy2`,
    `${utils.CLASSE_PREFIX}SXuIHpkWwVB810Mf`,
  ],
  2: [
    `${utils.CLASSE_PREFIX}7t8RfjNRBurxyp6a`,
  ],
  3: [
    `${utils.CLASSE_PREFIX}dVeLjxkksFYM7Wl6`,
    `${utils.CLASSE_PREFIX}QNVuXJmqpWVdzsEz`,
  ],
  4: [
    `${utils.CLASSE_PREFIX}G385iJnHjefiSrds`,
  ],
  6: [
    `${utils.CLASSE_PREFIX}GNHVlaUEwCga722D`,
  ],
  7: [
    `${utils.CLASSE_PREFIX}GRjgIjKcwQk2Kl7C`,
  ],
  10: [
    `${utils.CLASSE_PREFIX}vtotjCZT2Cq7adjz`,
  ],
  11: [
    `${utils.CLASSE_PREFIX}OiuXnmZMIsjhzXlx`,
  ],
  14: [
    `${utils.CLASSE_PREFIX}wh8iBuy4Yqh9dd9b`,
  ],
  18: [
    `${utils.CLASSE_PREFIX}HROGHK48H4SRdI6l`,
  ],
  20: [
    `${utils.CLASSE_PREFIX}eHUyLkapbEjtXmvr`,
  ],
});

export default {
  artificier,
  'artificier (alchimiste)': utils.concatenateClassFeatures(artificier, {
    3: [
      `${utils.CLASSE_PREFIX}WPoAGODvgkA51Rp2`,
      `${utils.CLASSE_PREFIX}Wvf2d7zVoKR5HUEa`,
      `${utils.CLASSE_PREFIX}1tbl6L4Cx0RHDaYZ`,
    ],
    5: [
      `${utils.CLASSE_PREFIX}PvkRDfrULuvxMBKq`,
    ],
    9: [
      `${utils.CLASSE_PREFIX}apfb2lhxmYhcpGHp`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}ueevx6GaXQGxtxiR`,
    ],
  }),
  'artificier (artilleur)': utils.concatenateClassFeatures(artificier, {
    3: [
      `${utils.CLASSE_PREFIX}fGl7FsznS4ves50M`,
      `${utils.CLASSE_PREFIX}rsjwranuBYq0okjU`,
      `${utils.CLASSE_PREFIX}ksuRxdW0lZjnLQJY`,
    ],
    5: [
      `${utils.CLASSE_PREFIX}EerNjqnyqmMv7RUm`,
    ],
    9: [
      `${utils.CLASSE_PREFIX}p9pFwZQl1pEtMzeH`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}nAqGa74cP6z4JM6i`,
    ],
  }),
  'artificier (forgeron de guerre)': utils.concatenateClassFeatures(
    artificier,
    {
      3: [
        `${utils.CLASSE_PREFIX}R4SD7e8MmkH15Rt3`,
        `${utils.CLASSE_PREFIX}Ati7ZAIoNWdX1grm`,
        `${utils.CLASSE_PREFIX}5QSdytsx5w6XiCGX`,
        `${utils.CLASSE_PREFIX}yJSQhIiOzoxfCbbU`,
      ],
      5: [
        `${utils.CLASSE_PREFIX}FdGHsGxwuuqD6ZdX`,
      ],
      9: [
        `${utils.CLASSE_PREFIX}oXfSUjmYWmGUnBYZ`,
      ],
      15: [
        `${utils.CLASSE_PREFIX}G6FKxKfzOeJ5aJlh`,
      ],
    },
  ),
  'artificier (armurier)': utils.concatenateClassFeatures(
    artificier,
    {
      3: [
        `${utils.CLASSE_PREFIX}fK3Jg9epP3KNB2aS`,
        `${utils.CLASSE_PREFIX}9tOLA1V4bsNyDrYp`,
        `${utils.CLASSE_PREFIX}wDRay049AK08pPsg`,
        `${utils.CLASSE_PREFIX}U1xXZxFuef6n6HHD`,
      ],
      5: [
        `${utils.CLASSE_PREFIX}FdGHsGxwuuqD6ZdX`,
      ],
      9: [
        `${utils.CLASSE_PREFIX}Ydk9KKVx3DN25BM1`,
      ],
      15: [
        `${utils.CLASSE_PREFIX}xrstBtMKZ6Vh49Rr`,
      ],
    },
  ),
};
