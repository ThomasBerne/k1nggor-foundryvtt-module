import utils from './utils.js';

const paladin = utils.featuresToClass({
  1: [
    `${utils.CLASSE_PREFIX}RCgrCxL2NJVTC4zd`,
    `${utils.CLASSE_PREFIX}jO8W6h4rePHIPakM`,
  ],
  2: [
    `${utils.CLASSE_PREFIX}kkIjrOukVs3WzWgK`,
    `${utils.CLASSE_PREFIX}FVRTCITogSj4dRbA`,
    `${utils.CLASSE_PREFIX}vk5Gb4sIvAGZXfNI`,
  ],
  3: [
    `${utils.CLASSE_PREFIX}VpPO3u5RQACu8YNh`,
    `${utils.CLASSE_PREFIX}NRAEDXRqBrCoiYxv`,
  ],
  4: [
    `${utils.CLASSE_PREFIX}G385iJnHjefiSrds`,
  ],
  5: [
    `${utils.CLASSE_PREFIX}FdGHsGxwuuqD6ZdX`,
  ],
  6: [
    `${utils.CLASSE_PREFIX}OYpVGIPiNKWBUy3B`,
  ],
  10: [
    `${utils.CLASSE_PREFIX}FFaVidRYNBUSRBJm`,
  ],
  11: [
    `${utils.CLASSE_PREFIX}KdXvc1hAVt0ZxkQN`,
  ],
  14: [
    `${utils.CLASSE_PREFIX}iQh3Cpa8nu4qf07P`,
  ],
});

export default {
  paladin,
  'paladin (serment de conquête)': utils.concatenateClassFeatures(paladin, {
    3: [
      `${utils.CLASSE_PREFIX}C0e3NBj4Kq5PEhUO`,
      `${utils.CLASSE_PREFIX}VrHDGFHaV9LvedwU`,
    ],
    7: [
      `${utils.CLASSE_PREFIX}3VsdchofRsiiS0UR`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}cDacV9gLEspP4Gwt`,
    ],
    20: [
      `${utils.CLASSE_PREFIX}wa6NeKllW45W8aBf`,
    ],
  }),
  'paladin (serment de dévotion)': utils.concatenateClassFeatures(paladin, {
    3: [
      `${utils.CLASSE_PREFIX}HaLUVmcvHQ6JqAtL`,
      `${utils.CLASSE_PREFIX}S8mgUHUxalDLuy6p`,
    ],
    7: [
      `${utils.CLASSE_PREFIX}MdbO7ViniaK5g7U2`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}OpDegcLXZkNlpPJP`,
    ],
    20: [
      `${utils.CLASSE_PREFIX}UTWjY4RkF6zbbuO7`,
    ],
  }),
  'paladin (serment de la couronne)': utils.concatenateClassFeatures(paladin, {
    3: [
      `${utils.CLASSE_PREFIX}suUEOl3crLITByLH`,
      `${utils.CLASSE_PREFIX}y2BCgBW4QYmmSqrA`,
      `${utils.CLASSE_PREFIX}Y5uHuqfxpwJpw3ZK`,
    ],
    7: [
      `${utils.CLASSE_PREFIX}Yi8m4ASpmyv34qxO`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}PyDt0cnGFTCDvxro`,
    ],
    20: [
      `${utils.CLASSE_PREFIX}PSEIB3fgqCkTrRiZ`,
    ],
  }),
  'paladin (serment de vengeance)': utils.concatenateClassFeatures(paladin, {
    3: [
      `${utils.CLASSE_PREFIX}CsNcEFr53IZydIs0`,
      `${utils.CLASSE_PREFIX}nuNPV9nJJ6LKOBdF`,
    ],
    7: [
      `${utils.CLASSE_PREFIX}4pUgbO7W1CrOgDdh`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}Yyu5E52UDLpEyeFI`,
    ],
    20: [
      `${utils.CLASSE_PREFIX}TBACnCm7LaM3Fy9d`,
    ],
  }),
  'paladin (serment de rédemption)': utils.concatenateClassFeatures(paladin, {
    3: [
      `${utils.CLASSE_PREFIX}yw5DMekSWCgopHlG`,
      `${utils.CLASSE_PREFIX}JwnO9VDBHlUiZ1Gw`,
    ],
    7: [
      `${utils.CLASSE_PREFIX}goDilPFJOntPNqg4`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}U2lahxn11gGVyWeX`,
    ],
    20: [
      `${utils.CLASSE_PREFIX}DBTapaz6bZwxuHf5`,
    ],
  }),
  'paladin (serment des anciens)': utils.concatenateClassFeatures(paladin, {
    3: [
      `${utils.CLASSE_PREFIX}oZ5VqlgOydiEqbWA`,
      `${utils.CLASSE_PREFIX}Wax6Sy0vWqflt02P`,
    ],
    7: [
      `${utils.CLASSE_PREFIX}GEGUv1LdleACX2pt`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}oi4c7nIczCS0PtEV`,
    ],
    20: [
      `${utils.CLASSE_PREFIX}89hgqqRoJ9gtehj7`,
    ],
  }),
};
