import utils from './utils.js';

const chasseur = utils.featuresToClass({
  1: [
    `${utils.CLASSE_PREFIX}89j0TW0edO7BnFIG`,
    `${utils.CLASSE_PREFIX}YPUx1CJOWv3aANca`,
  ],
  2: [
    `${utils.CLASSE_PREFIX}TrK10bfLXNGHD2oR`,
    `${utils.CLASSE_PREFIX}V8vUnoqbLFqXGUCH`,
  ],
  3: [
    `${utils.CLASSE_PREFIX}ixc2m35NJ8iDDnVv`,
  ],
  4: [
    `${utils.CLASSE_PREFIX}G385iJnHjefiSrds`,
  ],
  5: [
    `${utils.CLASSE_PREFIX}FdGHsGxwuuqD6ZdX`,
  ],
  6: [
    `${utils.CLASSE_PREFIX}79iOIX88cpKwpJz5`,
  ],
  11: [
    `${utils.CLASSE_PREFIX}i0nK036rk1XtyTgp`,
  ],
  13: [
    `${utils.CLASSE_PREFIX}4ICMLGRVd12cmfIt`,
  ],
  14: [
    `${utils.CLASSE_PREFIX}8S4FDWD5CnHsPzgG`,
  ],
  17: [
    `${utils.CLASSE_PREFIX}Z44hREwXrLMvzj6k`,
  ],
  20: [
    `${utils.CLASSE_PREFIX}1tpGVlmvOvobsbfF`,
  ],
});

export default {
  chasseur,
  'chasseur (grimm)': utils.concatenateClassFeatures(chasseur, {
    2: [
      `${utils.CLASSE_PREFIX}5hUZlMg6njPfSySc`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}jZfhBDW0yxct1I57`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}DeJUBDAo6jiKUL8D`,
      `${utils.CLASSE_PREFIX}Ka5vV3pz8Bl3RazH`,
    ],
    18: [
      `${utils.CLASSE_PREFIX}nvptfVH1IYW5zrAr`,
    ],
  }),
  'chasseur (guerre)': utils.concatenateClassFeatures(chasseur, {
    2: [
      `${utils.CLASSE_PREFIX}e9OSHi1hfVKgYZuy`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}5iTxbkZpHWpmQyeg`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}Voh3O1sUQtGCYq27`,
      `${utils.CLASSE_PREFIX}0uFwWY05XhLAp1m3`,
    ],
    18: [
      `${utils.CLASSE_PREFIX}Fc6qSpF7rnrDPQML`,
    ],
  }),
  'chasseur (ville)': utils.concatenateClassFeatures(chasseur, {
    2: [
      `${utils.CLASSE_PREFIX}96mMX5bx6VDdDF33`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}mDdaYrU5TyRSpxtw`,
      `${utils.CLASSE_PREFIX}VeTcyuK1fWHaYnW1`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}jtLwxZW1Y43snqk0`,
    ],
    18: [
      `${utils.CLASSE_PREFIX}yWU5s1lnlCQuiWrF`,
    ],
  }),
};
