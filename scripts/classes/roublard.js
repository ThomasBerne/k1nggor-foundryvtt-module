import utils from './utils.js';

const roublard = utils.featuresToClass({
  1: [
    `${utils.CLASSE_PREFIX}WSc1YgV34YzGYlhb`,
    `${utils.CLASSE_PREFIX}L27TBm7FORdchsA6`,
    `${utils.CLASSE_PREFIX}27r9K02Qf3ADbG8m`,
  ],
  2: [
    `${utils.CLASSE_PREFIX}KsBJwixrRNKnfGkI`,
  ],
  3: [
    `${utils.CLASSE_PREFIX}MiTxsDaf6HcB5T3d`,
  ],
  4: [
    `${utils.CLASSE_PREFIX}G385iJnHjefiSrds`,
  ],
  5: [
    `${utils.CLASSE_PREFIX}O4UCsk9GkBJn7nUd`,
  ],
  7: [
    `${utils.CLASSE_PREFIX}lNseUZy7IYvgTkSW`,
  ],
  11: [
    `${utils.CLASSE_PREFIX}QtL1AODZjjoJWJDQ`,
  ],
  14: [
    `${utils.CLASSE_PREFIX}dKcI3GDRaZtt5vWx`,
  ],
  15: [
    `${utils.CLASSE_PREFIX}zv6Z1MzaZn1rGwBS`,
  ],
  18: [
    `${utils.CLASSE_PREFIX}0XsTYZOR0klGSdhD`,
  ],
  20: [
    `${utils.CLASSE_PREFIX}QGFlixqGfUyfyk8E`,
  ],
});

export default {
  roublard,
  'roublard (voleur)': utils.concatenateClassFeatures(roublard, {
    3: [
      `${utils.CLASSE_PREFIX}tzPMbVsC0hErMTZX`,
      `${utils.CLASSE_PREFIX}44oGw69xt0qed2sP`,
    ],
    9: [
      `${utils.CLASSE_PREFIX}8DazLGWxPwtBVJhO`,
    ],
    13: [
      `${utils.CLASSE_PREFIX}cxHV42sGRAaqiiNa`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}55TGXGjBzuodwS8y`,
    ],
  }),
  'roublard (enquêteur)': utils.concatenateClassFeatures(roublard, {
    3: [
      `${utils.CLASSE_PREFIX}1s0e8NyFbpcUTU1k`,
      `${utils.CLASSE_PREFIX}xgigpzvyBUnwGO1m`,
      `${utils.CLASSE_PREFIX}fsRWVLrj9lLCog2y`,
    ],
    9: [
      `${utils.CLASSE_PREFIX}2wPhgkiw1hgzQfFd`,
    ],
    13: [
      `${utils.CLASSE_PREFIX}KaAppQalY7Jot9C6`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}qAGj6cMQfhIiQitZ`,
    ],
  }),
  'roublard (éclaireur)': utils.concatenateClassFeatures(roublard, {
    3: [
      `${utils.CLASSE_PREFIX}THXlgXlG2QT2gNyT`,
      `${utils.CLASSE_PREFIX}cvVBC3GtanYdJbz1`,
    ],
    9: [
      `${utils.CLASSE_PREFIX}8pLb8h0BNgkShVGb`,
    ],
    13: [
      `${utils.CLASSE_PREFIX}H2VwTZc05z1QH2Uw`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}ELXSwg9Fa6hmifFD`,
    ],
  }),
  'roublard (conspirateur)': utils.concatenateClassFeatures(roublard, {
    3: [
      `${utils.CLASSE_PREFIX}oE8Y48AqwXoujW2L`,
      `${utils.CLASSE_PREFIX}SNPf54KLa2KlG4z7`,
    ],
    9: [
      `${utils.CLASSE_PREFIX}rn31GbHGXuycw7nG`,
    ],
    13: [
      `${utils.CLASSE_PREFIX}6g1jH7DNTENtzJrw`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}SGQBKl8rN8XxI5qu`,
    ],
  }),
  'roublard (bretteur)': utils.concatenateClassFeatures(roublard, {
    3: [
      `${utils.CLASSE_PREFIX}zBaq7dq7iwQnVGHy`,
      `${utils.CLASSE_PREFIX}HlEoBWzfm82mppmP`,
    ],
    9: [
      `${utils.CLASSE_PREFIX}fJQkE5nJQNIGrV4F`,
    ],
    13: [
      `${utils.CLASSE_PREFIX}XvbvmYbXpmnxcElY`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}on9goVClSb2bsgq6`,
    ],
  }),
  'roublard (assassin)': utils.concatenateClassFeatures(roublard, {
    3: [
      `${utils.CLASSE_PREFIX}OG6gf90VDb3VhUko`,
      `${utils.CLASSE_PREFIX}le3K8c44dve53XKS`,
    ],
    9: [
      `${utils.CLASSE_PREFIX}PEwCFswCu6p0ol1L`,
    ],
    13: [
      `${utils.CLASSE_PREFIX}xnguSI8HMN1m6hvM`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}8gma5hCMz0zhPn1L`,
    ],
  }),
  'roublard (arnaqueur arcanique)': utils.concatenateClassFeatures(roublard, {
    3: [
      `${utils.CLASSE_PREFIX}QLOYrX1lssTCyGrY`,
      `${utils.CLASSE_PREFIX}S3shKSqX7sryjJUl`,
    ],
    9: [
      `${utils.CLASSE_PREFIX}xD6Sw2wGIY9p4a4l`,
    ],
    13: [
      `${utils.CLASSE_PREFIX}mR3tqgT5TD2J4gKB`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}IQSEVsPMBsJpQ8pv`,
    ],
  }),
  'roublard (fantôme)': utils.concatenateClassFeatures(roublard, {
    3: [
      `${utils.CLASSE_PREFIX}pj6pktXbwly2FUHU`,
      `${utils.CLASSE_PREFIX}pahYNVBGLaxkY6BQ`,
    ],
    9: [
      `${utils.CLASSE_PREFIX}EFjqUmywdENAp5aX`,
    ],
    13: [
      `${utils.CLASSE_PREFIX}nWFID4G3o63pMenB`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}Xv1c90ZaXC3mBjf7`,
    ],
  }),
  'roublard (coutelâme)': utils.concatenateClassFeatures(roublard, {
    3: [
      `${utils.CLASSE_PREFIX}Z5mC6JLRqufmF9qj`,
      `${utils.CLASSE_PREFIX}HgpMt6pYWlkbWosh`,
    ],
    9: [
      `${utils.CLASSE_PREFIX}PoaQ1w2EqFupVcor`,
    ],
    13: [
      `${utils.CLASSE_PREFIX}1yf5tjXSs9ksZLFg`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}zmvEcm6I0YFNFO65`,
    ],
  }),
};
