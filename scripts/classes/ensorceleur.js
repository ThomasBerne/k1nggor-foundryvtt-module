import utils from './utils.js';

const ensorceleur = utils.featuresToClass({
  1: [
    `${utils.CLASSE_PREFIX}qnL8IuXctTRMjGXf`,
    `${utils.CLASSE_PREFIX}3wOfaQEhCDXPf8cx`,
  ],
  2: [
    `${utils.CLASSE_PREFIX}otNUoNoOHycNbwRo`,
  ],
  3: [
    `${utils.CLASSE_PREFIX}SUnVuEn34uG5qtSl`,
  ],
  4: [
    `${utils.CLASSE_PREFIX}G385iJnHjefiSrds`,
  ],
  20: [
    `${utils.CLASSE_PREFIX}ATqZqxyo0SveXCWD`,
  ],
});

export default {
  ...ensorceleur,
  'ensorceleur (lignée draconique)': utils.concatenateClassFeatures(
    ensorceleur,
    {
      1: [
        `${utils.CLASSE_PREFIX}Mmty7ztST0t0WYLo`,
        `${utils.CLASSE_PREFIX}RLXQWfUITRIOLeaI`,
      ],
      6: [
        `${utils.CLASSE_PREFIX}lp2j7hwF1bEpg6nk`,
      ],
      14: [
        `${utils.CLASSE_PREFIX}rpgVbwsx0JqxI2MI`,
      ],
      18: [
        `${utils.CLASSE_PREFIX}PS5xk9cMiHTZPd2l`,
      ],
    },
  ),
  'ensorceleur (magie des ombres)': utils.concatenateClassFeatures(
    ensorceleur,
    {
      1: [
        `${utils.CLASSE_PREFIX}Gf4ojys1EY1EXowj`,
        `${utils.CLASSE_PREFIX}UnDYlhoUBTS6AbLA`,
      ],
      6: [
        `${utils.CLASSE_PREFIX}eoIgC7wkmv87moUg`,
      ],
      14: [
        `${utils.CLASSE_PREFIX}VRsdhDGkO5AkbGDK`,
      ],
      18: [
        `${utils.CLASSE_PREFIX}H60xRcy9PMcEnPBq`,
      ],
    },
  ),
  'ensorceleur (sorcellerie des tempêtes)': utils.concatenateClassFeatures(
    ensorceleur,
    {
      1: [
        `${utils.CLASSE_PREFIX}txaUbSVjnquaLds7`,
        `${utils.CLASSE_PREFIX}vHNVtqS5LxJkMxtL`,
      ],
      6: [
        `${utils.CLASSE_PREFIX}CCD9lJkBuNXAi3PS`,
        `${utils.CLASSE_PREFIX}TDcQDVNzTqvfTdzr`,
      ],
      14: [
        `${utils.CLASSE_PREFIX}GK0cDnCJv5rZ1dih`,
      ],
      18: [
        `${utils.CLASSE_PREFIX}H60xRcy9PMcEnPBq`,
      ],
    },
  ),
  'ensorceleur (âme divine)': utils.concatenateClassFeatures(ensorceleur, {
    1: [
      `${utils.CLASSE_PREFIX}JkAQA7gRm9YaG4WP`,
      `${utils.CLASSE_PREFIX}IJHZaPOXqJThtV7J`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}8gWqALOHohFwmIzC`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}zlTTmTRwqTDmrLGw`,
    ],
    18: [
      `${utils.CLASSE_PREFIX}h4E0iHcaEunaOde8`,
    ],
  }),
  'ensorceleur (âme du mécanisme)': utils.concatenateClassFeatures(
    ensorceleur,
    {
      1: [
        `${utils.CLASSE_PREFIX}FAvVUpeMTarqjfdh`,
        `${utils.CLASSE_PREFIX}mJvH6wxbEkMAO769`,
      ],
      6: [
        `${utils.CLASSE_PREFIX}PTh06kMgZmqD6EZL`,
      ],
      14: [
        `${utils.CLASSE_PREFIX}4M0TFRIivaE0VX3V`,
      ],
      18: [
        `${utils.CLASSE_PREFIX}rw2ggBp5HrPecbWh`,
      ],
    },
  ),
  'ensorceleur (âme psionique)': utils.concatenateClassFeatures(ensorceleur, {
    1: [
      `${utils.CLASSE_PREFIX}CupFxCpKzJbfvBQG`,
      `${utils.CLASSE_PREFIX}zCI7rgBB8fp8EG5v`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}0FrnxE8MsW5MbEs5`,
      `${utils.CLASSE_PREFIX}Cms4g2Hf4k7dPkWW`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}FGm6wopgXE0pqxmT`,
    ],
    18: [
      `${utils.CLASSE_PREFIX}Lq74pXC7hSacqf3o`,
    ],
  }),
  'ensorceleur (magie sauvage)': utils.concatenateClassFeatures(ensorceleur, {
    1: [
      `${utils.CLASSE_PREFIX}FX1vOvFtuOJjKeZp`,
      `${utils.CLASSE_PREFIX}0FY0PFwSJaWLehhG`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}grogXtZbwBeTmzbD`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}GoFYMDsGD29bvly9`,
    ],
    18: [
      `${utils.CLASSE_PREFIX}Pc5vn6idlwRkPBc3`,
    ],
  }),
};
