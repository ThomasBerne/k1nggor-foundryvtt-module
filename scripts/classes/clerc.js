import utils from './utils.js';

const clerc = utils.featuresToClass({
  1: [
    `${utils.CLASSE_PREFIX}YG9Adpv3Z1yeVrKh`,
    `${utils.CLASSE_PREFIX}eGuIScV11MdHtx5R`,
  ],
  2: [
    `${utils.CLASSE_PREFIX}Uc2XGSqm1lRaOUBN`,
    `${utils.CLASSE_PREFIX}ESAxNXec6LIk7dIn`,
  ],
  4: [
    `${utils.CLASSE_PREFIX}G385iJnHjefiSrds`,
  ],
  5: [
    `${utils.CLASSE_PREFIX}3U7MiIPMjzMYwdfV`,
  ],
  10: [
    `${utils.CLASSE_PREFIX}tzxZGSjHYfEdUbOi`,
  ],
});

export default {
  ...clerc,
  "clerc (domaine de l'ordre)": utils.concatenateClassFeatures(clerc, {
    1: [
      `${utils.CLASSE_PREFIX}2NaLeTU6dwIb9aJm`,
      `${utils.CLASSE_PREFIX}KwYO0m3mmtxRslln`,
      `${utils.CLASSE_PREFIX}8HFNfUYEnezgNVvP`,
    ],
    2: [
      `${utils.CLASSE_PREFIX}PwARWpi174OPIuix`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}5Hn2o68riFSmT0u4`,
    ],
    8: [
      `${utils.CLASSE_PREFIX}UYntz8BRHBGVYIma`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}0hpoAeaaqdznefvg`,
    ],
  }),
  'clerc (domaine de la guerre)': utils.concatenateClassFeatures(clerc, {
    1: [
      `${utils.CLASSE_PREFIX}dlMHVgVksiMrcYRM`,
      `${utils.CLASSE_PREFIX}vT7D0ZbTflzkYO7h`,
      `${utils.CLASSE_PREFIX}fQF9AnfnfAvR6S2p`,
    ],
    2: [
      `${utils.CLASSE_PREFIX}ZVOcHkSVjYocI0YD`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}NO2S2W1R5KsHuZg7`,
    ],
    8: [
      `${utils.CLASSE_PREFIX}97YA4qfj4a6LsKeS`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}B6XDLA3WbLHLP9Xm`,
    ],
  }),
  'clerc (domaine de la lumière)': utils.concatenateClassFeatures(clerc, {
    1: [
      `${utils.CLASSE_PREFIX}fWVcPJaFLwZJXLOH`,
      `${utils.CLASSE_PREFIX}hEnorkk9niFuPhRr`,
    ],
    2: [
      `${utils.CLASSE_PREFIX}pd523Af1u9RYP2Xb`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}xF5g0sT9P6K5I5Ow`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}106sFD89yoQRAf3S`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}2Lkx0h9su0vdve1W`,
    ],
  }),
  'clerc (domaine de la nature)': utils.concatenateClassFeatures(clerc, {
    1: [
      `${utils.CLASSE_PREFIX}51YjgcwqPbNmW6a4`,
      `${utils.CLASSE_PREFIX}fvuSaxDHIP0Dyl1i`,
      `${utils.CLASSE_PREFIX}sr8Vx9EUsJJPcfsH`,
    ],
    2: [
      `${utils.CLASSE_PREFIX}3bEHYtb854iYXqNj`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}oTmjo0JZ5eemJsgV`,
    ],
    8: [
      `${utils.CLASSE_PREFIX}SuD5DVkupL9uhufQ`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}X7WiJy7KpuYXbFJG`,
    ],
  }),
  'clerc (domaine de la ruse)': utils.concatenateClassFeatures(clerc, {
    1: [
      `${utils.CLASSE_PREFIX}pKeeH3B4pXOwDyN8`,
      `${utils.CLASSE_PREFIX}Fspgta0gfgaDLUZV`,
    ],
    2: [
      `${utils.CLASSE_PREFIX}Fspgta0gfgaDLUZV`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}rBpdPd6ud7z1D9ln`,
    ],
    8: [
      `${utils.CLASSE_PREFIX}7NOmqK4w0yDIUqxG`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}5sBJgJWuYRc8D4SE`,
    ],
  }),
  'clerc (domaine de la tempête)': utils.concatenateClassFeatures(clerc, {
    1: [
      `${utils.CLASSE_PREFIX}sV0srldq3JGd1e5Q`,
      `${utils.CLASSE_PREFIX}ev4NikSdOd82oxJp`,
      `${utils.CLASSE_PREFIX}YaV8ELCCLo7UlLRZ`,
    ],
    2: [
      `${utils.CLASSE_PREFIX}rxTLTbTw4uv3XsKW`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}5RFYAiCj8jfUx8ty`,
    ],
    8: [
      `${utils.CLASSE_PREFIX}qldqHDoLjjUtwoYU`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}yO4SjuaWFvHHaRet`,
    ],
  }),
  'clerc (domaine de la tombe)': utils.concatenateClassFeatures(clerc, {
    1: [
      `${utils.CLASSE_PREFIX}eSG8CvbKFUPrfQaV`,
      `${utils.CLASSE_PREFIX}mxylQZuCIXRiUdeG`,
      `${utils.CLASSE_PREFIX}K1I4YrtbQstOIOAm`,
    ],
    2: [
      `${utils.CLASSE_PREFIX}4Fi5YCWa8b1urrPo`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}3JVvtw4NVuffFvCo`,
    ],
    8: [
      `${utils.CLASSE_PREFIX}0yWWBHPXe1j6pclF`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}lvw6Qdzgi3PTGJ2K`,
    ],
  }),
  'clerc (domaine de la vie)': utils.concatenateClassFeatures(clerc, {
    1: [
      `${utils.CLASSE_PREFIX}cVwvDqJnALvENCo4`,
      `${utils.CLASSE_PREFIX}pjCNUpkpcvNGcfrO`,
      `${utils.CLASSE_PREFIX}77IWNnI5suS30d5w`,
    ],
    2: [
      `${utils.CLASSE_PREFIX}fFyjRi5PLFTdA6Pp`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}lhZ8EOzCppYtV8yZ`,
    ],
    8: [
      `${utils.CLASSE_PREFIX}TcCYftCPYya0XBhE`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}00QjNIi0GfhC7ACh`,
    ],
  }),
  'clerc (domaine des arcanes)': utils.concatenateClassFeatures(clerc, {
    1: [
      `${utils.CLASSE_PREFIX}Panapn6tZUUZC1MF`,
      `${utils.CLASSE_PREFIX}5DVCVsqhH0svp65J`,
    ],
    2: [
      `${utils.CLASSE_PREFIX}kMfcI2oUnjXQ6wsz`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}xOEkQygHBjHZQhGy`,
    ],
    8: [
      `${utils.CLASSE_PREFIX}33KNpLF5Cdhohp0w`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}LgfcO98LInqSNLT8`,
    ],
  }),
  'clerc (domaine du crépuscule)': utils.concatenateClassFeatures(clerc, {
    1: [
      `${utils.CLASSE_PREFIX}BwzczIXamYmGXpMl`,
      `${utils.CLASSE_PREFIX}cT1ZhgpYJRE5LPC6`,
      `${utils.CLASSE_PREFIX}f1fY3LKnpGCGEcjH`,
      `${utils.CLASSE_PREFIX}Bbzj430RJyBPRH5P`,
    ],
    2: [
      `${utils.CLASSE_PREFIX}3gH6DGz6aUN6r0aA`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}KtXf43mD1IUAO3j3`,
    ],
    8: [
      `${utils.CLASSE_PREFIX}yQIJgcgbSDtu7OC4`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}RHn3xHicoiqSv4V2`,
    ],
  }),
  'clerc (domaine du savoir)': utils.concatenateClassFeatures(clerc, {
    1: [
      `${utils.CLASSE_PREFIX}Ilh6JjBz0VigtPD5`,
      `${utils.CLASSE_PREFIX}MOddxfGYBemVZSoB`,
    ],
    2: [
      `${utils.CLASSE_PREFIX}vSBu0Vh9lCOdVijN`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}jR2A18dNulaip3of`,
    ],
    8: [
      `${utils.CLASSE_PREFIX}6Vg9IZLxCJCgTqcH`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}LToebNRsB8j0WVtD`,
    ],
  }),
  'clerc (domaine de la forge)': utils.concatenateClassFeatures(clerc, {
    1: [
      `${utils.CLASSE_PREFIX}shS6NUNOd0agCacn`,
      `${utils.CLASSE_PREFIX}K0QrjA3XUOvy0RYH`,
      `${utils.CLASSE_PREFIX}Fa4MnIzjg1umsCzW`,
    ],
    2: [
      `${utils.CLASSE_PREFIX}3Qs92h6HpEsOOVT1`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}Kqe72255Qxk0rkMS`,
    ],
    8: [
      `${utils.CLASSE_PREFIX}MHnTuBgUXxH1ukxd`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}ydz2lXQl0VUxccas`,
    ],
  }),
};
