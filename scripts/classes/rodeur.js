import utils from './utils.js';

const rodeur = utils.featuresToClass({
  1: [
    `${utils.CLASSE_PREFIX}O0f0vigEfsSc6FfE`,
    `${utils.CLASSE_PREFIX}czTRum0kgQgNi72n`,
  ],
  2: [
    `${utils.CLASSE_PREFIX}s8F95Q7rO0z1P79J`,
    `${utils.CLASSE_PREFIX}TzAiw9wbEHwOZXcx`,
  ],
  3: [
    `${utils.CLASSE_PREFIX}3BEO4gOogduh7UBO`,
    `${utils.CLASSE_PREFIX}NdBNryi6RxwQzzj0`,
  ],
  4: [
    `${utils.CLASSE_PREFIX}G385iJnHjefiSrds`,
  ],
  6: [
    `${utils.CLASSE_PREFIX}C3f9er6pjI9PGhCU`,
  ],
  8: [
    `${utils.CLASSE_PREFIX}CDRyBjn5aKuTqXpA`,
  ],
  10: [
    `${utils.CLASSE_PREFIX}sJaq8hyyweJdBvKC`,
  ],
  14: [
    `${utils.CLASSE_PREFIX}icatLC4vIon4xkRG`,
  ],
  18: [
    `${utils.CLASSE_PREFIX}E3zJ4YSpIaBaNhar`,
  ],
  20: [
    `${utils.CLASSE_PREFIX}OFChhf0x9xh5KVIY`,
  ],
});

export default {
  rôdeur: rodeur,
  'rôdeur (conclave des chasseurs)': utils.concatenateClassFeatures(rodeur, {
    3: [
      `${utils.CLASSE_PREFIX}sVIIwS0KyjlRvUpk`,
    ],
    5: [
      `${utils.CLASSE_PREFIX}FdGHsGxwuuqD6ZdX`,
    ],
    7: [
      `${utils.CLASSE_PREFIX}nSD9nkUsIaObERnZ`,
    ],
    11: [
      `${utils.CLASSE_PREFIX}1LnWKGfbseTOY8Gy`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}Qe8GACpVFqyHzDIU`,
    ],
  }),
  "rôdeur (arpenteur de l'horizon)": utils.concatenateClassFeatures(rodeur, {
    3: [
      `${utils.CLASSE_PREFIX}xVWj2emRAd27alro`,
      `${utils.CLASSE_PREFIX}QynaWROwjT8obCGl`,
      `${utils.CLASSE_PREFIX}j4cVbkSw12G6emxF`,
    ],
    5: [
      `${utils.CLASSE_PREFIX}FdGHsGxwuuqD6ZdX`,
    ],
    7: [
      `${utils.CLASSE_PREFIX}0UVOs9Zeyukg7SUp`,
    ],
    11: [
      `${utils.CLASSE_PREFIX}lNv63CnJeFEjsXnK`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}Baj2XNMul70Oqws0`,
    ],
  }),
  'rôdeur (conclave des bêtes)': utils.concatenateClassFeatures(rodeur, {
    3: [
      `${utils.CLASSE_PREFIX}vGeQiqd5UjJ2jO12`,
    ],
    5: [
      `${utils.CLASSE_PREFIX}LZ4YOWeo0OPxGGFF`,
    ],
    7: [
      `${utils.CLASSE_PREFIX}PSRb2Y7zTBpenOH1`,
    ],
    11: [
      `${utils.CLASSE_PREFIX}9G8JAfpvkkQ7tI41`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}X7OOceiGGJs8PfwW`,
    ],
  }),
  'rôdeur (conclave des profondeurs)': utils.concatenateClassFeatures(rodeur, {
    3: [
      `${utils.CLASSE_PREFIX}ogai9ge5jYOX8GMk`,
      `${utils.CLASSE_PREFIX}ogai9ge5jYOX8GMk`,
    ],
    5: [
      `${utils.CLASSE_PREFIX}FdGHsGxwuuqD6ZdX`,
    ],
    7: [
      `${utils.CLASSE_PREFIX}nsEk3RSeXpWxJmvT`,
    ],
    11: [
      `${utils.CLASSE_PREFIX}OWc3ZWPGH7rnrPmy`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}jV2KJK5cF9s4Thfm`,
    ],
  }),
  'rôdeur (vagabond féérique)': utils.concatenateClassFeatures(rodeur, {
    3: [
      `${utils.CLASSE_PREFIX}wW8oC9VUUnxeZ7HJ`,
      `${utils.CLASSE_PREFIX}fq8booegORLCXly4`,
      `${utils.CLASSE_PREFIX}idRNp5b2h6r3xGqf`,
    ],
    5: [
      `${utils.CLASSE_PREFIX}FdGHsGxwuuqD6ZdX`,
    ],
    7: [
      `${utils.CLASSE_PREFIX}bXHim03pBrCeAvlv`,
    ],
    11: [
      `${utils.CLASSE_PREFIX}0PeUK03oHE6rRqZD`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}OZUdnZmxMBp5D7c4`,
    ],
  }),
  'rôdeur (gardien des nuées)': utils.concatenateClassFeatures(rodeur, {
    3: [
      `${utils.CLASSE_PREFIX}WuF3HDt1zVayGHIp`,
    ],
    5: [
      `${utils.CLASSE_PREFIX}FdGHsGxwuuqD6ZdX`,
    ],
    7: [
      `${utils.CLASSE_PREFIX}ij1itI7B6lmLL4Wc`,
    ],
    11: [
      `${utils.CLASSE_PREFIX}4sQxVnQjynhRkzOm`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}EpebQQVLs2Rxu9X0`,
    ],
  }),
  'rôdeur (tueur de monstres)': utils.concatenateClassFeatures(rodeur, {
    3: [
      `${utils.CLASSE_PREFIX}lKwSybJ5mqp7zxUm`,
      `${utils.CLASSE_PREFIX}JT00r8E0qACTMGBb`,
      `${utils.CLASSE_PREFIX}qxkzzJ1DLYUzBjyk`,
    ],
    5: [
      `${utils.CLASSE_PREFIX}FdGHsGxwuuqD6ZdX`,
    ],
    7: [
      `${utils.CLASSE_PREFIX}71JpW6HCI4VKaQMv`,
    ],
    11: [
      `${utils.CLASSE_PREFIX}4hcgmUTYLK2RrLhV`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}gAvFS6vAoWCl4Qkp`,
    ],
  }),
};
