import utils from './utils.js';

const magicien = utils.featuresToClass({
  1: [
    `${utils.CLASSE_PREFIX}lXeuNQILBrzIZg8U`,
    `${utils.CLASSE_PREFIX}Hp2fNhPakA4KTXmU`,
  ],
  2: [
    `${utils.CLASSE_PREFIX}5cSwjqfyi6XaaJ8z`,
  ],
  4: [
    `${utils.CLASSE_PREFIX}G385iJnHjefiSrds`,
  ],
  18: [
    `${utils.CLASSE_PREFIX}A2Hd9YoEPTtbd5Mw`,
  ],
  20: [
    `${utils.CLASSE_PREFIX}NowcU9o4nB7YvTCX`,
  ],
});

export default {
  magicien,
  'magicien (chantelame)': utils.concatenateClassFeatures(magicien, {
    2: [
      `${utils.CLASSE_PREFIX}qmYMHGhTEx0DJqtv`,
      `${utils.CLASSE_PREFIX}1Svxad8bA358nwwS`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}NlYM81QCouQOaveX`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}OipbjYj1bIjMkyLf`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}dRQKylV7xP3lRvun`,
    ],
  }),
  'magicien (magie de guerre)': utils.concatenateClassFeatures(magicien, {
    2: [
      `${utils.CLASSE_PREFIX}cxj1PrHR1d0Le9vg`,
      `${utils.CLASSE_PREFIX}mhw9NtrV37axga5f`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}oqGa0vg3HGB1Qjiv`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}m6P09pa5DAYFWAHE`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}gqPSJZOU7bTKZlZt`,
    ],
  }),
  'magicien (ordre des scribes)': utils.concatenateClassFeatures(magicien, {
    2: [
      `${utils.CLASSE_PREFIX}DkodahuvsECgwa9F`,
      `${utils.CLASSE_PREFIX}JqikpVyNwrQGBhgE`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}sCtr1WBYnDPcXGKm`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}xldCMeituhvkA6hb`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}2o458KlOIqUn8d4w`,
    ],
  }),
  "magicien (école de l'abjuration)": utils.concatenateClassFeatures(magicien, {
    2: [
      `${utils.CLASSE_PREFIX}t7plNo8viBvyNp00`,
      `${utils.CLASSE_PREFIX}SCpTyEpm63NNSykJ`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}uDJqnnnKXISacpXC`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}slqdfhoV6GXesUbD`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}UqybDJI1009w8172`,
    ],
  }),
  "magicien (école de l'enchantement)": utils.concatenateClassFeatures(
    magicien,
    {
      2: [
        `${utils.CLASSE_PREFIX}YuP8cMCsxqQdfdS7`,
        `${utils.CLASSE_PREFIX}EBGrV6bJCM3PIMH9`,
      ],
      6: [
        `${utils.CLASSE_PREFIX}Pv5p3TL5NpzWCRK0`,
      ],
      10: [
        `${utils.CLASSE_PREFIX}8FzzVjVH9QnVPKs4`,
      ],
      14: [
        `${utils.CLASSE_PREFIX}UabjOiPTDwOU5SuQ`,
      ],
    },
  ),
  "magicien (école de l'illusion)": utils.concatenateClassFeatures(magicien, {
    2: [
      `${utils.CLASSE_PREFIX}mbFmsxmTH2dwo8X6`,
      `${utils.CLASSE_PREFIX}MnHC4jrGHxvakQPC`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}gcNzWpprYO7ecQCk`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}4SKsC5HPyiACkSkW`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}iSisEzaUZmkMP3N2`,
    ],
  }),
  "magicien (école de l'évocation)": utils.concatenateClassFeatures(magicien, {
    2: [
      `${utils.CLASSE_PREFIX}XHmk0hBwLHY5jcPR`,
      `${utils.CLASSE_PREFIX}twDhtGaJnD4irCM2`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}QNGaU8grMBwQjuZX`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}7g98e4xaQrytpwkh`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}pk7aPmdeipdqXS92`,
    ],
  }),
  'magicien (école de la divination)': utils.concatenateClassFeatures(
    magicien,
    {
      2: [
        `${utils.CLASSE_PREFIX}MrOOVhi5FT5XV1K5`,
        `${utils.CLASSE_PREFIX}jbcOIDZMVE0GGetE`,
      ],
      6: [
        `${utils.CLASSE_PREFIX}xvkE8yOTqTD1RclU`,
      ],
      10: [
        `${utils.CLASSE_PREFIX}VgpEb1ZR6NVPcUac`,
      ],
      14: [
        `${utils.CLASSE_PREFIX}K9VhKzJ72S63x78x`,
      ],
    },
  ),
  'magicien (école de la nécromancie)': utils.concatenateClassFeatures(
    magicien,
    {
      2: [
        `${utils.CLASSE_PREFIX}GZZ656TTjhbaFTkU`,
        `${utils.CLASSE_PREFIX}qsaKxzHFPVCsmnKA`,
      ],
      6: [
        `${utils.CLASSE_PREFIX}ry9FcBpk1G52EmB3`,
      ],
      10: [
        `${utils.CLASSE_PREFIX}Indzdz0ivLXzHUD5`,
      ],
      14: [
        `${utils.CLASSE_PREFIX}cLh2M8iXyoRLdcsR`,
      ],
    },
  ),
  'magicien (école de la transmutation)': utils.concatenateClassFeatures(
    magicien,
    {
      2: [
        `${utils.CLASSE_PREFIX}CXPeamUkpTiqPJ9D`,
        `${utils.CLASSE_PREFIX}Q3LgJKrHdpBheRBt`,
      ],
      6: [
        `${utils.CLASSE_PREFIX}YWXES4PLb3NQN769`,
      ],
      10: [
        `${utils.CLASSE_PREFIX}s9EnGbCjkC2bznwI`,
      ],
      14: [
        `${utils.CLASSE_PREFIX}bGZheGPHVDxJj1tj`,
      ],
    },
  ),
};
