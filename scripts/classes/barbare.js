import utils from './utils.js';

const barbare = utils.featuresToClass({
  1: [
    `${utils.CLASSE_PREFIX}ANlP5Hfs2jZev4vA`,
    `${utils.CLASSE_PREFIX}Lg5i9qI3Q93gzaRg`,
  ],
  2: [
    `${utils.CLASSE_PREFIX}vy4RXEmpUI7D9xs6`,
    `${utils.CLASSE_PREFIX}aNflpz2ITnrSYrgA`,
  ],
  3: [
    `${utils.CLASSE_PREFIX}tXm11Noi3Xyd0Klk`,
  ],
  4: [
    `${utils.CLASSE_PREFIX}G385iJnHjefiSrds`,
  ],
  5: [
    `${utils.CLASSE_PREFIX}FdGHsGxwuuqD6ZdX`,
    `${utils.CLASSE_PREFIX}edzPSz0YGQwbuFy3`,
  ],
  7: [
    `${utils.CLASSE_PREFIX}MxHGjEysU5sCvUiw`,
  ],
  9: [
    `${utils.CLASSE_PREFIX}i9s9vEguR0kjbZW1`,
  ],
  11: [
    `${utils.CLASSE_PREFIX}8BGFx6cYycIYWvcr`,
  ],
  15: [
    `${utils.CLASSE_PREFIX}F3HZJrzBDMYcCOAh`,
  ],
  18: [
    `${utils.CLASSE_PREFIX}zYlmxo5VX2kNdMjM`,
  ],
  20: [
    `${utils.CLASSE_PREFIX}wJmprU6WBcrgjodq`,
  ],
});

export default {
  barbare,
  'barbare (voie de la bête)': utils.concatenateClassFeatures(barbare, {
    3: [
      `${utils.CLASSE_PREFIX}zZ902qlvkmLGcysk`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}g9u5ke2PbT8ZwD9L`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}qtTxXddDr5oxPDfs`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}R4F7o4kTEpkli2GQ`,
    ],
  }),
  'barbare (voie de la magie sauvage)': utils.concatenateClassFeatures(
    barbare,
    {
      3: [
        `${utils.CLASSE_PREFIX}66S3IOylk5Tp19Xk`,
        `${utils.CLASSE_PREFIX}xABzvqwjJNpMH3pe`,
      ],
      6: [
        `${utils.CLASSE_PREFIX}3FAqSYQ2gvQDUoIB`,
      ],
      10: [
        `${utils.CLASSE_PREFIX}9E1CGFmN5iZIHfUX`,
      ],
      14: [
        `${utils.CLASSE_PREFIX}ymT4zMJErB9XK93a`,
      ],
    },
  ),
  'barbare (voie de la berserker)': utils.concatenateClassFeatures(barbare, {
    3: [
      `${utils.CLASSE_PREFIX}yX5stNE1SXxMyBk3`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}OBxMqRHmP2FOMg81`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}tM8vUrVJqvk8p7ph`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}3YhSm8B2WsXLUL33`,
    ],
  }),
  'barbare (voie du zélote)': utils.concatenateClassFeatures(barbare, {
    3: [
      `${utils.CLASSE_PREFIX}tSb71l2rnh1DZ88G`,
      `${utils.CLASSE_PREFIX}sKszDplPzY80cm7B`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}sKszDplPzY80cm7B`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}z9WuqdJH3C3Oaiac`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}DW89cGlqbFYWyNTN`,
    ],
  }),
  'barbare (voie du fou de guerre)': utils.concatenateClassFeatures(barbare, {
    3: [
      `${utils.CLASSE_PREFIX}dreMOsslAVPWZIv9`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}iBT2usVzqcB7bqjg`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}zlV5OUDNB8g1nKw4`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}XV1K02isIZVblmqT`,
    ],
  }),
  'barbare (voie du gardien ancestral)': utils.concatenateClassFeatures(
    barbare,
    {
      3: [
        `${utils.CLASSE_PREFIX}QoAyYoAaghI3pXg7`,
      ],
      6: [
        `${utils.CLASSE_PREFIX}gx14OEhpx7roYbHu`,
      ],
      10: [
        `${utils.CLASSE_PREFIX}cETiCVTPHut0Bj9A`,
      ],
      14: [
        `${utils.CLASSE_PREFIX}AQZcwGaOVytLVgWi`,
      ],
    },
  ),
  'barbare (voie du guerrier totémique)': utils.concatenateClassFeatures(
    barbare,
    {
      3: [
        `${utils.CLASSE_PREFIX}XXDPOBiQpJFWtdvG`,
        `${utils.CLASSE_PREFIX}vLdaBf9opK3RtFBn`,
      ],
      6: [
        `${utils.CLASSE_PREFIX}DIuKnReK9xpE98GS`,
      ],
      10: [
        `${utils.CLASSE_PREFIX}M2SoVhBHosfpBdB2`,
      ],
      14: [
        `${utils.CLASSE_PREFIX}4ptYrO7bcdaeKdDc`,
      ],
    },
  ),
  'barbare (voie du héraut des tempêtes)': utils.concatenateClassFeatures(
    barbare,
    {
      3: [
        `${utils.CLASSE_PREFIX}GENoI4s9CMvgpnWu`,
      ],
      6: [
        `${utils.CLASSE_PREFIX}Ogh5MNf2IcpP1S0x`,
      ],
      10: [
        `${utils.CLASSE_PREFIX}4sdV1EcnyBuyzlJK`,
      ],
      14: [
        `${utils.CLASSE_PREFIX}z3tXmLy6EWrGgpRA`,
      ],
    },
  ),
};
