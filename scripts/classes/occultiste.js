import utils from './utils.js';

const occultiste = utils.featuresToClass({
  1: [
    `${utils.CLASSE_PREFIX}4E3VMhcyD83NxYur`,
    `${utils.CLASSE_PREFIX}tw6tn9BORWKXbV3K`,
  ],
  2: [
    `${utils.CLASSE_PREFIX}GsUBidGRAApAPm5q`,
  ],
  3: [
    `${utils.CLASSE_PREFIX}ScurSO9xzTG4gUgN`,
  ],
  4: [
    `${utils.CLASSE_PREFIX}G385iJnHjefiSrds`,
  ],
  11: [
    `${utils.CLASSE_PREFIX}b5eJ0K2M9rAdHbp4`,
  ],
  20: [
    `${utils.CLASSE_PREFIX}vkPnydM0IoGPNErq`,
  ],
});

export default {
  occultiste,
  'occultiste (archifée)': utils.concatenateClassFeatures(occultiste, {
    1: [
      `${utils.CLASSE_PREFIX}ieMnzurQSyLJXCDv`,
      `${utils.CLASSE_PREFIX}QTj9P4AbmpJ8HbRM`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}bXkujZ0SmVSq4CGT`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}8IYfAcL3ubOdBMDb`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}Q2VFpxfZ60dwX7jZ`,
    ],
  }),
  'occultiste (céleste)': utils.concatenateClassFeatures(occultiste, {
    1: [
      `${utils.CLASSE_PREFIX}mxTONEfuDJFdVjSB`,
      `${utils.CLASSE_PREFIX}dxODHputd13vGyWO`,
      `${utils.CLASSE_PREFIX}QH1Fjxz2P1fW36BK`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}T7LzJ5QuZcApWS2w`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}FxQEtEG3TJ6oOHQP`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}edwoTxJ8XOLdaBP9`,
    ],
  }),
  'occultiste (fiélon)': utils.concatenateClassFeatures(occultiste, {
    1: [
      `${utils.CLASSE_PREFIX}IXpqtINhnSGLKKXl`,
      `${utils.CLASSE_PREFIX}xLQ6GpIaq3OT0R4g`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}TGvcBqjTppuXRdsv`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}ywAgQPh2dU1bkwz1`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}CnF5uQ7bWRIqU5mD`,
    ],
  }),
  'occultiste (grand ancien)': utils.concatenateClassFeatures(occultiste, {
    1: [
      `${utils.CLASSE_PREFIX}KmfRT1os2PC2L3lR`,
      `${utils.CLASSE_PREFIX}MpERk8bYBOPXyqXx`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}bcmQ5j9u5LDjjAk3`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}09npwudoeN2PsZIo`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}lSlIaYOx3T5OEQZF`,
    ],
  }),
  'occultiste (magelame)': utils.concatenateClassFeatures(occultiste, {
    1: [
      `${utils.CLASSE_PREFIX}JmtMnAdgQVIntLEv`,
      `${utils.CLASSE_PREFIX}IfbJwyph7Uj1wHIV`,
      `${utils.CLASSE_PREFIX}3zZEesa3Qo9xYA5z`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}to0MWTlW1GCDy6R3`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}lJoNiozq2e0onmKB`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}RFvdwNVRupjXPN5p`,
    ],
  }),
  "occultiste (l'immortel)": utils.concatenateClassFeatures(occultiste, {
    1: [
      `${utils.CLASSE_PREFIX}iUyEVdueI0hCAUF8`,
      `${utils.CLASSE_PREFIX}xlcyRG38Sk9g3NAx`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}oArv9NltEtvJ5PJq`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}DowyChTXwmH0qqep`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}STLRCkUR7xs7JHfQ`,
    ],
  }),
  'occultiste (le génie)': utils.concatenateClassFeatures(occultiste, {
    1: [
      `${utils.CLASSE_PREFIX}MGpfH53bG8yYirxN`,
      `${utils.CLASSE_PREFIX}KweJV76pu2046WyT`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}mP5uk4YdXFsgyRFK`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}1XOqmMRb8ayyNQJn`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}tkRGsgl471TMoELN`,
    ],
  }),
};
