export function featuresToClass(features) {
  return {
    features,
    subclasses: {},
  };
}

export function concatenateClassFeatures(classe, subClasseFeaturesList) {
  const features = { ...classe.features };

  for (let index = 1; index <= 20; index++) {
    const classeValue = classe.features[index];
    const subClasseValue = subClasseFeaturesList[index];
    if (classeValue?.length && subClasseValue?.length) {
      features[index] = [...classeValue, ...subClasseValue];
    } else if (!classeValue?.length && subClasseValue?.length) {
      features[index] = subClasseValue;
    }
  }

  return featuresToClass(features);
}

export const CLASSE_PREFIX = 'Compendium.dnd5e-fr-full-version.dandd-5e-capacites-de-classe.';

export default {
  featuresToClass,
  concatenateClassFeatures,
  CLASSE_PREFIX
};
