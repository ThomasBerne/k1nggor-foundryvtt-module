import utils from './utils.js';

const pugiliste = utils.featuresToClass({
  1: [
    `${utils.CLASSE_PREFIX}PUjKJIvZKKmpViar`,
    `${utils.CLASSE_PREFIX}4t1hSzMOOIJIctOE`,
  ],
  2: [
    `${utils.CLASSE_PREFIX}Q1ADNjoh9dhr0u2O`,
  ],
  3: [
    `${utils.CLASSE_PREFIX}GpGrDEHsSqlunWBa`,
  ],
  4: [
    `${utils.CLASSE_PREFIX}G385iJnHjefiSrds`,
  ],
  5: [
    `${utils.CLASSE_PREFIX}FdGHsGxwuuqD6ZdX`,
  ],
  6: [
    `${utils.CLASSE_PREFIX}j760O0hX4TXASNGS`,
  ],
  7: [
    `${utils.CLASSE_PREFIX}khFTHomsPb3SVWIA`,
  ],
  9: [
    `${utils.CLASSE_PREFIX}wYg8QHfFulANLpw9`,
  ],
  11: [
    `${utils.CLASSE_PREFIX}QFwvM2XvddHH8VjP`,
  ],
  14: [
    `${utils.CLASSE_PREFIX}pACmkpct60ndMHrT`,
  ],
  20: [
    `${utils.CLASSE_PREFIX}rtArrbw3hZ5gSFCw`,
  ],
});

export default {
  pugiliste,
  'pugiliste (catcheur)': utils.concatenateClassFeatures(pugiliste, {
    3: [
      `${utils.CLASSE_PREFIX}PUjKJIvZKKmpViar`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}PUjKJIvZKKmpViar`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}PUjKJIvZKKmpViar`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}PUjKJIvZKKmpViar`,
    ],
  }),
  'pugiliste (boxeur)': utils.concatenateClassFeatures(pugiliste, {
    3: [
      `${utils.CLASSE_PREFIX}t9lrCHhUzBbpQ5th`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}DmM5cS6o0yBvTBEr`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}xdgP5bVy6lno5RC6`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}xmTjhNx1tc1tzIyb`,
    ],
  }),
  'pugiliste (combattant de rue)': utils.concatenateClassFeatures(pugiliste, {
    3: [
      `${utils.CLASSE_PREFIX}KKWF9podVCFeYowc`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}YrSi0iERghcQKp6O`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}yynxMrVOFiG0XB7a`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}Rsmkt53KGrl3ty6V`,
    ],
  }),
};
