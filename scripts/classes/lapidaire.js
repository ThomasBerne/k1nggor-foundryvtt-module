import utils from './utils.js';

const lapidaire = utils.featuresToClass({
  1: [
    `${utils.CLASSE_PREFIX}eyMRhy6NWEb5ggFQ`,
    `${utils.CLASSE_PREFIX}YTzyxFJv6bM3i5gr`,
  ],
  2: [
    `${utils.CLASSE_PREFIX}CU3FmVOFqyWQ0VJe`,
  ],
  3: [
    `${utils.CLASSE_PREFIX}PeY7lESwk6WA4RPf`,
  ],
  4: [
    `${utils.CLASSE_PREFIX}G385iJnHjefiSrds`,
  ],
  7: [
    `${utils.CLASSE_PREFIX}5gBTETepi42B5WQo`,
  ],
  11: [
    `${utils.CLASSE_PREFIX}osoIGuFgnV1lMlhn`,
    `${utils.CLASSE_PREFIX}mlFnEuVy93X7nJST`,
  ],
  15: [
    `${utils.CLASSE_PREFIX}ixwWmdSRJhizjZ5W`,
  ],
  18: [
    `${utils.CLASSE_PREFIX}APx62M1MYfCKfZO0`,
  ],
  20: [
    `${utils.CLASSE_PREFIX}l4r8U3zOqDqsOd8G`,
  ],
});

export default {
  lapidaire,
  'lapidaire (guerrier-gemme)': utils.concatenateClassFeatures(lapidaire, {
    2: [
      `${utils.CLASSE_PREFIX}CU3FmVOFqyWQ0VJe`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}NlYM81QCouQOaveX`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}4aHyJZlIRVll8ac4`,
      `${utils.CLASSE_PREFIX}Z6A1QDaB3iaBR0yB`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}NCrxI8dUN6epY3LS`,
    ],
  }),
  'lapidaire (sculpteurs de jade)': utils.concatenateClassFeatures(lapidaire, {
    2: [
      `${utils.CLASSE_PREFIX}qAdtGOVl8ftSCuWa`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}Ow7CQkO2IljFuWk0`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}O0cbwJf9sHUop9S3`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}Vr78Nx73jAvZNjL1`,
    ],
  }),
  'lapidaire (5 éclats divin)': utils.concatenateClassFeatures(lapidaire, {
    2: [
      `${utils.CLASSE_PREFIX}z58j07BJHb3wT26Z`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}T4ZO34RF1f51xZtT`,
    ],
    10: [
      `${utils.CLASSE_PREFIX}xyVQgrqeOVijpgG2`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}c1KfkEZF4IG4RvVH`,
    ],
  }),
};
