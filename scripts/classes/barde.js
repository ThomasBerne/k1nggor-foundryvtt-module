import utils from './utils.js';

const barde = utils.featuresToClass({
  1: [
    `${utils.CLASSE_PREFIX}kfOartWzM0gh3077`,
    `${utils.CLASSE_PREFIX}hzyiYxiQvQThLy9v`,
  ],
  2: [
    `${utils.CLASSE_PREFIX}uo3C0Cbl2vRkGUrq`,
    `${utils.CLASSE_PREFIX}JxdcGwwmIJfubDp5`,
  ],
  3: [
    `${utils.CLASSE_PREFIX}nf47A8wyAexFrDjQ`,
    `${utils.CLASSE_PREFIX}hu8yqBpg3UNzbRpg`,
  ],
  4: [
    `${utils.CLASSE_PREFIX}G385iJnHjefiSrds`,
  ],
  5: [
    `${utils.CLASSE_PREFIX}asTOc4lUnNaeL4Yx`,
  ],
  6: [
    `${utils.CLASSE_PREFIX}W2bRwVpctadoEjkS`,
  ],
  10: [
    `${utils.CLASSE_PREFIX}h1pIfAiWsYEq7TlZ`,
  ],
  20: [
    `${utils.CLASSE_PREFIX}nXUisKJHYK1angie`,
  ],
});

export default {
  barde,
  "barde (collège de l'éloquence)": utils.concatenateClassFeatures(barde, {
    3: [
      `${utils.CLASSE_PREFIX}PoILcienPPWHF0wB`,
      `${utils.CLASSE_PREFIX}28WBBTO6nnQ0T4Et`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}5mdT30DhaLmZd0Br`,
      `${utils.CLASSE_PREFIX}QhzzTS2lp8zXAD3X`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}kjpwD9LoneGKIcID`,
    ],
  }),
  'barde (collège de la création)': utils.concatenateClassFeatures(barde, {
    3: [
      `${utils.CLASSE_PREFIX}Y7813xt4Qdp4CupD`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}57dCyKzfUB0boyle`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}SJbqzrYdlmpBm5IM`,
    ],
  }),
  'barde (collège de la séduction)': utils.concatenateClassFeatures(barde, {
    3: [
      `${utils.CLASSE_PREFIX}q6n28d7Pw4gyjbxo`,
      `${utils.CLASSE_PREFIX}jMQGQvb9SqZGFEnt`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}VVIMUCSft6nnb318`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}hvSca5T2xWetSTxJ`,
    ],
  }),
  'barde (collège de la vaillance)': utils.concatenateClassFeatures(barde, {
    3: [
      `${utils.CLASSE_PREFIX}KcJm4xHaVlbpPXso`,
      `${utils.CLASSE_PREFIX}iBrKuAPSiSFuTAb8`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}NlYM81QCouQOaveX`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}xmorZi7Au0I7nGOZ`,
    ],
  }),
  'barde (collège des murmures)': utils.concatenateClassFeatures(barde, {
    3: [
      `${utils.CLASSE_PREFIX}3Y3s2ayhiTA2xNz1`,
      `${utils.CLASSE_PREFIX}YGbRkwIdBGa4Rq2G`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}udJWlibyyZcxyrie`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}tEVlcfTEtObL9mzO`,
    ],
  }),
  'barde (collège des épées)': utils.concatenateClassFeatures(barde, {
    3: [
      `${utils.CLASSE_PREFIX}GJPNdS2Kj6myNyjb`,
      `${utils.CLASSE_PREFIX}K1fDg8AUd8iFo0RX`,
      `${utils.CLASSE_PREFIX}qkSlyYDlaDsZEeAH`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}NlYM81QCouQOaveX`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}gqUFckWcIgRTpY4N`,
    ],
  }),
  'barde (collège du savoir)': utils.concatenateClassFeatures(barde, {
    3: [
      `${utils.CLASSE_PREFIX}4Sz6GiHBEFD7kuDQ`,
      `${utils.CLASSE_PREFIX}zBHWDM6Gbprrrhm0`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}izLWbUA6nghWvhmW`,
    ],
    14: [
      `${utils.CLASSE_PREFIX}usLNt3wiPhAP6FZm`,
    ],
  }),
};
