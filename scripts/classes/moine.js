import utils from './utils.js';

const moine = utils.featuresToClass({
  1: [
    `${utils.CLASSE_PREFIX}oADlC15qjgGhjlVo`,
    `${utils.CLASSE_PREFIX}f2QiEomN0Sjfhun1`,
  ],
  2: [
    `${utils.CLASSE_PREFIX}qgOIyaCp8RG02zID`,
    `${utils.CLASSE_PREFIX}a3Wn8XRGeND47zhk`,
  ],
  3: [
    `${utils.CLASSE_PREFIX}agxGOs6evD3FIOsA`,
    `${utils.CLASSE_PREFIX}EAgfrADbXW92Ddgf`,
  ],
  4: [
    `${utils.CLASSE_PREFIX}G385iJnHjefiSrds`,
    `${utils.CLASSE_PREFIX}K18S1s5j5rLbEDhS`,
  ],
  5: [
    `${utils.CLASSE_PREFIX}FdGHsGxwuuqD6ZdX`,
    `${utils.CLASSE_PREFIX}kPkMfBXg6UrWZQQi`,
  ],
  6: [
    `${utils.CLASSE_PREFIX}GjPCWPdAWoQuM9CJ`,
  ],
  7: [
    `${utils.CLASSE_PREFIX}ap0v795Fi5B6OWJf`,
    `${utils.CLASSE_PREFIX}tekBXXQAkOwrQiHJ`,
  ],
  10: [
    `${utils.CLASSE_PREFIX}MpaU2uZnaF96Ht3y`,
  ],
  13: [
    `${utils.CLASSE_PREFIX}mgFJqNnjX0d2ueVS`,
  ],
  14: [
    `${utils.CLASSE_PREFIX}dJp4PA3ipb5CBhNT`,
  ],
  15: [
    `${utils.CLASSE_PREFIX}dJp4PA3ipb5CBhNT`,
  ],
});

export default {
  moine,
  "moine (voie de l'ombre)": utils.concatenateClassFeatures(moine, {
    3: [
      `${utils.CLASSE_PREFIX}0LSihAaECwNToyYf`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}jAx638EOVlRw9rxX`,
    ],
    11: [
      `${utils.CLASSE_PREFIX}RSVhT2VtHGxJSNc8`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}ygb23UwyNUnbXXbI`,
    ],
  }),
  "moine (voie de l'âme solaire)": utils.concatenateClassFeatures(moine, {
    3: [
      `${utils.CLASSE_PREFIX}wAHOqO45PTvQIZXx`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}gEPUU4rdHmApGxBt`,
    ],
    11: [
      `${utils.CLASSE_PREFIX}z7RxXNvUxzXRCZ2d`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}1v3dAAR0YBHHwzrz`,
    ],
  }),
  'moine (voie de la longue mort)': utils.concatenateClassFeatures(moine, {
    3: [
      `${utils.CLASSE_PREFIX}cPDaBETMGELs0MSb`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}5mXSoNSpr86xvPbQ`,
    ],
    11: [
      `${utils.CLASSE_PREFIX}FaG2S0pJ3iBIFlPF`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}CmmSyd5gqtbTttW5`,
    ],
  }),
  'moine (voie du kensei)': utils.concatenateClassFeatures(moine, {
    3: [
      `${utils.CLASSE_PREFIX}uQPYMn8ZqzBHAVTP`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}m2aGIaSxTF5nHBtJ`,
    ],
    11: [
      `${utils.CLASSE_PREFIX}4Je2k2v7XtoCJVkO`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}NHsu8ss09LRpTlAz`,
    ],
  }),
  'moine (voie du maître ivre)': utils.concatenateClassFeatures(moine, {
    3: [
      `${utils.CLASSE_PREFIX}Qjr3BSDybFYlVrKO`,
      `${utils.CLASSE_PREFIX}G0qEZDldCJfOCD5o`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}boIaPC5tunEXjwO0`,
    ],
    11: [
      `${utils.CLASSE_PREFIX}9tBWN8UtkVOjsfrW`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}8MJCiHsQ0x3hcpb1`,
    ],
  }),
  'moine (voie des quatre éléments)': utils.concatenateClassFeatures(moine, {
    3: [
      `${utils.CLASSE_PREFIX}B9Y6gb0Qh6DqUIy2`,
    ],
  }),
  'moine (voie de la miséricorde)': utils.concatenateClassFeatures(moine, {
    3: [
      `${utils.CLASSE_PREFIX}rHE8e5p24C7iuzJH`,
      `${utils.CLASSE_PREFIX}twiOEFQTllwWcKr0`,
      `${utils.CLASSE_PREFIX}hiT9xC1LuKG0VsmR`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}dPkXqLkg6K9O4lld`,
    ],
    11: [
      `${utils.CLASSE_PREFIX}jjn0zLw11nsjKV4T`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}pSpWaIpEHDXASi6L`,
    ],
  }),
  'moine (voie du double astral)': utils.concatenateClassFeatures(moine, {
    3: [
      `${utils.CLASSE_PREFIX}2SUYw2da6s8Kxjaa`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}iIvlyJnqfa0zoJtC`,
    ],
    11: [
      `${utils.CLASSE_PREFIX}dyp2GLJatLgtcTq2`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}067ZHYXCTGLjLL86`,
    ],
  }),
  "moine (voie de la paume)": utils.concatenateClassFeatures(moine, {
    3: [
      `${utils.CLASSE_PREFIX}9Lp3Gaj91AEtF1yE`,
    ],
    6: [
      `${utils.CLASSE_PREFIX}gLRhr48k8qoBXAqf`,
    ],
    11: [
      `${utils.CLASSE_PREFIX}jo9gyMReqtY9T9LS`,
    ],
    17: [
      `${utils.CLASSE_PREFIX}nBwCQffvBFCS2TnT`,
    ],
  }),
};
