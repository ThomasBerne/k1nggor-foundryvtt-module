import utils from './utils.js';

const hémomancien = utils.featuresToClass({
  1: [
    `${utils.CLASSE_PREFIX}qoZCsmKOLLinVtmk`,
    `${utils.CLASSE_PREFIX}fTVDTsRdmGpk2qb6`,
  ],
  2: [
    `${utils.CLASSE_PREFIX}YtpkSe9z68nVtvOA`,
    `${utils.CLASSE_PREFIX}YbWNaCtu2vKR4Ohw`,
  ],
  3: [
    `${utils.CLASSE_PREFIX}I3DPaqMZg6m0GwUF`,
  ],
  4: [
    `${utils.CLASSE_PREFIX}G385iJnHjefiSrds`,
  ],
  5: [
    `${utils.CLASSE_PREFIX}FdGHsGxwuuqD6ZdX`,
  ],
  6: [
    `${utils.CLASSE_PREFIX}XxZo8QFeCRPrWiJk`,
  ],
  9: [
    `${utils.CLASSE_PREFIX}CW6dQqM19IKi5H9Y`,
  ],
  10: [
    `${utils.CLASSE_PREFIX}ITKKZG2prXDiqEVA`,
  ],
  13: [
    `${utils.CLASSE_PREFIX}7jOlgdwketDhr4yE`,
  ],
  14: [
    `${utils.CLASSE_PREFIX}fTC4ZcQ6EvZFoTka`,
  ],
  20: [
    `${utils.CLASSE_PREFIX}TmEJSAc2aLDAOjHm`,
  ],
});

export default {
  hémomancien,
  "hémomancien (ordre des spectricides)": utils.concatenateClassFeatures(barde, {
    3: [
      `${utils.CLASSE_PREFIX}4jNtCPgiLeX9NmKT`,
      `${utils.CLASSE_PREFIX}jjEkpsHioFbbaJEU`,
    ],
    7: [
      `${utils.CLASSE_PREFIX}opNyXQjoaRZP9mkp`,
    ],
    11: [
      `${utils.CLASSE_PREFIX}Yf62bPnUIheRp3Dd`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}j3r4GW4se7vhI6li`,
    ],
    18: [
      `${utils.CLASSE_PREFIX}mIxfNINK0cIMZjIH`,
    ],
  }),
  "hémomancien (ordre des lycans)": utils.concatenateClassFeatures(barde, {
    3: [
      `${utils.CLASSE_PREFIX}eRZNqOT1Bdw1aiUM`,
      `${utils.CLASSE_PREFIX}MYTtgWdDoGsZzOyZ`,
    ],
    7: [
      `${utils.CLASSE_PREFIX}9WSgDGjW9jk6C6PN`,
    ],
    11: [
      `${utils.CLASSE_PREFIX}FfXUvBDJkPP4odMC`,
    ],
    15: [
      `${utils.CLASSE_PREFIX}0AbRQAXdimgqodj5`,
    ],
    18: [
      `${utils.CLASSE_PREFIX}aoGn6o6aOdwSbws0`,
    ],
  }),
};
