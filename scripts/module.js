import artificerClasses from './classes/artificier.js';
import barbarianClasses from './classes/barbare.js';
import bardeClasses from './classes/barde.js';
import clercClasses from './classes/clerc.js';
import druideClasses from './classes/druide.js';
import ensorceleurClasses from './classes/ensorceleur.js';
import guerrierClasses from './classes/guerrier.js';
import magicienClasses from './classes/magicien.js';
import moineClasses from './classes/moine.js';
import occultisteClasses from './classes/occultiste.js';
import paladinClasses from './classes/paladin.js';
import roublardClasses from './classes/roublard.js';
import rodeurClasses from './classes/rodeur.js';
import chasseurClasses from './classes/chasseur.js';
import lapidaireClasses from './classes/lapidaire.js';
import pugilisteClasses from './classes/pugiliste.js';

Hooks.on('ready', () => {
  CONFIG.DND5E.classFeatures = {
    ...artificerClasses,
    ...barbarianClasses,
    ...bardeClasses,
    ...clercClasses,
    ...druideClasses,
    ...ensorceleurClasses,
    ...guerrierClasses,
    ...magicienClasses,
    ...moineClasses,
    ...occultisteClasses,
    ...paladinClasses,
    ...roublardClasses,
    ...rodeurClasses,
    ...chasseurClasses,
    ...lapidaireClasses,
    ...pugilisteClasses,
  };
});
